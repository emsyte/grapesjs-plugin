import pluginRegistration from './components/registration-form';
import pluginTemplateConfigurations from './components/template-configurations';
import pluginCountdown from './components/countdown/index';
import pluginImportTemplate from './components/import-template';
import pluginVariables from './components/variables';

export default (editor, opts = {}) => {
  let pfx = editor.getConfig('stylePrefix');

  let config = {
    addEmsyteBtn: 1,
    className: 'emsyte-grapejs',
    btnApplyLabel: 'Apply Template',
    btnExpLabel: 'Export Template',
    filenamePfx: 'grapesjs_template',
    filename: null,
    root: {
      css: {
        'style.css': ed => ed.getCss(),
      },
      'index.html': ed =>
        `<!doctype html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="./css/style.css">
          </head>
          <body>${ed.getHtml()}</body>
        <html>`,
    },
    isBinary: null,
    ...opts,
  };

  const {
    countdownOpts,
    registerOpts,
  } = config;

  pluginImportTemplate(editor, config);
  pluginTemplateConfigurations(editor, config);
  pluginRegistration(editor, registerOpts);
  pluginVariables(editor, config);
  pluginCountdown(editor, countdownOpts);

  // Clean Up Buttons on Panel
  // ---------------
  editor.Panels.removeButton("options","gjs-open-import-webpage")
  editor.Panels.removeButton("options","sw-visibility")
  editor.Panels.removeButton("options","canvas-clear")

      

  // Load Components & Assign Positions
  // --------------
  // editor.on("load", ()=>{
  //     // Hacky Way to Get Template Title on Top ~ Do Not Touch
  //     // const categoryBlock = document.getElementsByClassName("gjs-blocks-cs")[0];
  //     // const no_cat = document.getElementsByClassName("gjs-blocks-no-cat")[0];
  //     // categoryBlock.prepend(no_cat);

  //     // Close Down Categories To Make the UX Better
  //     editor.BlockManager.getCategories().forEach((c) => c.set("open", false));
  // })


};