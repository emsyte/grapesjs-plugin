export default function ImportTemplate(editor, opts) {
    let config = opts;
    let pfx = editor.getConfig('stylePrefix');



    // Add blocks
    let btnApply = document.createElement('button');

    // Utilizing Export Styles 
    let container  = document.createElement('div');
    container.className = 'gjs-export-dl'

    // Code Block
    let code = document.createElement('div');
    code.id=`${pfx}cm-code`;

    // HTML - Styles Block
    let html_container = document.createElement('div');
    html_container.className = `${pfx}cm-editor-c`;
    html_container.id = `${pfx}cm-htmlmixed`;

    let html_wrapper = document.createElement('div');
    html_wrapper.className = `${pfx}cm-editor`;
    html_wrapper.id = `${pfx}cm-css`;

    let html_title = document.createElement('div');
    html_title.id=`${pfx}cm-title`;
    html_title.textContent='HTML';

    html_container.appendChild(html_wrapper);
    html_wrapper.appendChild(html_title);
    html_wrapper.appendChild(code);

    let html_area = document.createElement('textarea');
    html_wrapper.appendChild(html_area)

    // CSS - Stlyes Block
    let css_container = document.createElement('div');
    css_container.className = `${pfx}cm-editor-c`;

    let css_wrapper = document.createElement('div');
    css_wrapper.className = `${pfx}cm-editor`;
    css_wrapper.id = `${pfx}cm-css`;

    let css_title = document.createElement('div');
    css_title.id=`${pfx}cm-title`;
    css_title.textContent='CSS'

    css_container.appendChild(css_wrapper);
    css_wrapper.appendChild(css_title);
    css_wrapper.appendChild(code);

    let css_area = document.createElement('textarea')
    css_wrapper.appendChild(css_area)

    // Code Viewers
    // --------------
    var htmlCodeViewer = editor.CodeManager.getViewer('CodeMirror').clone()
    var cssCodeViewer = editor.CodeManager.getViewer('CodeMirror').clone()

    htmlCodeViewer.set({
      codeName: 'htmlmixed',
      readOnly: 0,
      theme: 'hopscotch',
      autoBeautify: true,
      autoCloseTags: true,
      autoCloseBrackets: true,
      lineWrapping: true,
      styleActiveLine: true,
      smartIndent: true,
      indentWithTabs: true,
    })

    cssCodeViewer.set({
      codeName: 'css',
      readOnly: 0,
      theme: 'hopscotch',
      autoBeautify: true,
      autoCloseTags: true,
      autoCloseBrackets: true,
      lineWrapping: true,
      styleActiveLine: true,
      smartIndent: true,
      indentWithTabs: true
    })

    // Actions 
    btnApply.innerHTML = config.btnApplyLabel;
    btnApply.className = `${pfx}btn-prim`;
    btnApply.onclick = () => {
      editor.runCommand('apply-template');
    };


    let btnGenerateTemplate = document.createElement('button');
    btnGenerateTemplate.innerHTML = 'Generate Template';
    btnGenerateTemplate.className = `${pfx}btn-prim`;
    btnGenerateTemplate.style.float = "right";
    btnGenerateTemplate.onclick = () => {
      editor.runCommand('generate-template');
    };  
    

    // Panels
    // --------------
    editor.Panels.removeButton("options", "export-template");

    editor.Panels.addButton("options", [
      {
        id: "import",
        className: 'fa fa-code',
        command: 'import-template',
        attributes: { title: 'Modify Code' }
      },
      
    ]);

    // Commands
    // --------------
    editor.Commands.add('apply-template', {
      run(editor) {
        let html = htmlCodeViewer.editor.getValue()
        let css = cssCodeViewer.editor.getValue()
        editor.DomComponents.getWrapper().set('content', '')
        editor.setComponents(html.trim())
        editor.setStyle(css)
        editor.Modal.close()
      }
    });

    editor.Commands.add('generate-template', {
      run(editor) {
        let obj = {};
        obj.assets = [];
        obj.components = editor.getComponents();
        obj.html = editor.getHtml();
        obj.css = editor.getCss();
        obj.styles = editor.getStyle();
        console.log(obj)
      }
    });
    
    editor.Commands.add('import-template', {
      run(editor) {
        let htmlViewer = htmlCodeViewer.editor
        let cssViewer = cssCodeViewer.editor

        if (!htmlViewer && !cssViewer) {
          container.appendChild(html_container)
          container.appendChild(css_container)

          htmlCodeViewer.init(html_area)
          htmlViewer = htmlCodeViewer.editor

          cssCodeViewer.init(css_area)
          cssViewer = cssCodeViewer.editor
        }

        let html = editor.getHtml()
        let css = editor.getCss()

        editor.Modal.setContent('')
        editor.Modal.setContent(container)

        htmlCodeViewer.setContent(html)
        cssCodeViewer.setContent(css)
        editor.Modal.getContentEl().appendChild(btnApply);
        (config.localhost) && editor.Modal.getContentEl().appendChild(btnGenerateTemplate);
        // editor.Modal.getContentEl().appendChild(btnExp);

        editor.Modal.open({ title: 'Import Template'})

        htmlViewer.refresh()
        cssViewer.refresh()


      }
    });
 

}
