export default function TemplateConfigurations(editor, opts) {
  // Constants
  // --------------
  let page = opts.template.name || ""; //?Keep track of page names
  let templateName = opts.template.parent.name || "";
  const base = opts.template.parent.base;
  const $ = document.querySelectorAll.bind(document);

  // Html Wrappers
  // --------------
  const thumbs = (id, thumb) => {
    return `<div class="gjs-templates-card" data-id="${id}">
        ${thumb}
      </div>`;
  };
  const thumbCont = (thumbs) => {
    return `<div class="gjs-templates-card-2">
        ${thumbs}
      </div>`;
  };
  const templates = (thumbCont) => {
    return `<div id="templates" class="gjs-templates gjs-one-bg gjs-two-color">
        <div class="gjs-templates-overlay"></div>
        <div class="gjs-templates-cont">
          <div class="gjs-templates-header2"></div>
          ${thumbCont}
          <div class="gjs-templates-card-3">
            <div class="gjs-fonts" style="width:100%; margin-left: 20%;margin-top: 20%">
              <div style="width: 75%; display: inline-block;" class="gjs-field">
                <input style="border: 1px solid rgba(0, 0, 0, 0.2);
                  border-radius: 5px;
                  font-family: Arial, Helvetica, sans-serif;" type="text" placeholder="Template Name" value="${templateName}" name="pageName"  id="page-name">
              </div>
              <span style="display: block;">
                <button class="btn btn-secondary" ${
                  base ? "disabled" : ""
                } style="padding: 10px;
                  border-radius: 3px;
                  font-size: 0.9rem;
                  margin-left: 20%;
                  margin-top: 10px;" id="update-existing-template">Update existing Template</button>

                  <button class="btn btn-secondary" style="padding: 10px;
                  border-radius: 3px;
                  font-size: 0.9rem;
                  margin-left: 20%;
                  margin-top: 10px;" id="create-new-template">Save as new Template</button>
              </span>
            </div>
          </div>
          </div>
      </div>`;
  };

  // Helper Functions
  // --------------
  const render = () => {
    let thumbnailsEl = "";
    const dataSvg = `<iframe style="border: none; width: 100%; height: 220px; background: white;" id="thumbnail-container"></iframe>`;
    const thumbnailEl = `${dataSvg}<div class="label">Preview</div>`;
    (() => (thumbnailsEl += thumbs("Demo", thumbnailEl)))();
    return templates(thumbCont(thumbnailsEl));
  };

  // Panels
  // --------------
  editor.Panels.addButton("options", [
    {
      id: "save",
      className: "fa fa-floppy-o",
      label: "",
      command: () => {
        const button = editor.Panels.getPanel("options").buttons.get("save");
        button.set("className", "fa fa-circle-o-notch fa-spin");
        editor.store(() => {
          button.set("className", "fa fa-floppy-o");
        });
      },
      attributes: {
        title: "Save",
      },
    },
    {
      id: "save-as-global",
      className: "btn btn-success btn-sm",
      label: "Save as Global Template",
      command: "open-template-modal",
      attributes: {
        title: "Save",
        style:
          "font-weight: 400; padding: 5px 10px !important; line-height: 1 !important;",
      },
    },
    {
      id: "reset",
      className: "btn btn-warning btn-sm",
      label: "Reset",
      command: "reset",
      attributes: {
        title: "Save",
        style:
          "font-weight: 400; padding: 5px 10px !important; line-height: 1 !important;",
      },
    },
    {
      id: "exit",
      className: "btn btn-primary btn-sm",
      label: "Exit",
      command: "exit",
      attributes: {
        title: "Exit",
        style:
          "font-weight: 400; padding: 5px 10px  !important; line-height: 1 !important;",
      },
    },
  ]);

  // Commands
  // --------------
  // editor.Commands.add('save-template', {
  //     run: (editor, sender) => {
  //         sender && sender.set("active", 0); // turn off the button
  //         const className = sender.get("className");
  //         sender.set("className", className + " btn-loading");
  //         const el = editor.getWrapper().getEl();
  //         console.log('saving')
  //         return getJpeg(el, {
  //           quality: .005,
  //           height: 1000,
  //           'cacheBust': true,
  //           style: {
  //             'background-color': el.style.backgroundColor || 'white',
  //           },
  //         });
  //     }
  // });

  editor.Commands.add("open-template-modal", {
    run: async (editor, sender) => {
      const mdlClass = "gjs-mdl-dialog-tml";
      const mdlDialog = document.querySelector(".gjs-mdl-dialog");
      mdlDialog.classList.add(mdlClass);
      mdlDialog.style.marginTop = "15%";
      sender && sender.set && sender.set("active");
      editor.Modal.setTitle('<div style="font-size: 1rem">Save Temple</div>');
      editor.Modal.setContent(
        '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>'
      );

      editor.Modal.setContent(render(editor));
      document.getElementById("thumbnail-container").contentDocument
        .write(`<svg xmlns="http://www.w3.org/2000/svg" class="template-preview" viewBox="0 0 1300 1100" width="99%" height="220">
      <foreignObject width="100%" height="100%" style="pointer-events:none">
        <div xmlns="http://www.w3.org/1999/xhtml">
        ${editor.getHtml() + "<style scoped>" + editor.getCss() + "</style>"}
        </div>
      </foreignObject>
    </svg>`);

      $(".gjs-templates-card").forEach(
        (elm) =>
          elm.dataset.id == templateName &&
          elm.classList.add("gjs-templates-card-active")
      );

      $("#page-name")[0].addEventListener(
        "keyup",
        (e) => (page = e.currentTarget.value)
      );

      $("#create-new-template")[0].addEventListener("click", (e) => {
        templateName = $("#page-name")[0].value;
        editor.Commands.run("create-new-template", { templateName });
      });

      $("#update-existing-template")[0].addEventListener("click", (e) => {
        templateName = $("#page-name")[0].value;
        editor.Commands.run("update-existing-template", {
          templateName,
        });
      });

      editor.Modal.open();
      editor.Modal.getModel().once("change:open", () => {
        document.querySelector(".gjs-mdl-collector").innerHTML = "";
        mdlDialog.classList.remove(mdlClass);
      });

      return;
    },
  });

  editor.Commands.add("create-new-template", {
    run: (editor, sender) => {
      const button = $("#create-new-template")[0];
      button.disabled = true;
      const loader = document.createElement("div");
      loader.className = "spinner-border spinner-border-sm mr-2";
      button.prepend(loader);

      return { templateName };
    },
  });

  editor.Commands.add("update-existing-template", {
    run: (editor, sender) => {
      const button = $("#update-existing-template")[0];
      button.disabled = true;
      const loader = document.createElement("div");
      loader.className = "spinner-border spinner-border-sm mr-2";
      button.prepend(loader);
      return { templateName };
    },
  });

  editor.Commands.add("reset", {
    run: (editor, sender) => {},
  });

  // Blocks
  // --------------
  // editor.BlockManager.add("templateTitle", {
  //     label: `Template Title`,
  //     category: "",
  //     content: "",
  //     attributes: { class: " border-0", style: "width: 100%; color: white" },
  //     render: () => {
  //       return `
  //         <div>
  //           <h5 style={{ color: "#bbb" }}>${title}</h5>
  //           <input
  //             id="templateTitle"
  //             type="text"
  //             className="p-1"
  //             style={{ fontSize: "1.25em" }}
  //             placeholder="Template Title"
  //             required
  //           />
  //           <button>
  //             <i class="fa fa-save"></i>
  //           </button>
  //         </div>`
  //         },
  //     // script: function () {
  //     //     // Here I'm in another context
  //     //     // This part is might be the one stored in DB and then printed to
  //     //     // the end user in some 'Home page'.
  //     //     // Here, no one knows about GrapesJS editor, either `temp` or `fnName`
  //     //     console.log(temp, fnName); // are both undefined

  //     //     // The only way to "print" something from the model
  //     //     // is to use interpolation
  //     //     console.log('{[ customProp ]}'); // will print 'custom-value'

  //     //     // But this works ONLY because before storing the string the
  //     //     // editor replaces it with the property, so in DB it will store:
  //     //     console.log('custom-value');
  //     // },
  // });
}
