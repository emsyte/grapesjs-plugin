const registerButtonRef = "register-button";

export default function RegisterButton(editor, opts) {
  // Add blocks
  editor.BlockManager.add('emsyte-registration-form', {
    label: "Register Button",
    category: "Webinar",
    attributes: { class: "fa fa-wpforms" },
    content: `
    <div>
        <button
          type="button"
          data-webinarId="{{ webinarHash }}"
          class="btn btn-primary register-btn"
          id="justwebinar-registration"
        >
        <text>Register Now</text>
      </button>
    </div>
    `,
  });
}
